#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>

#include "globals.hpp"
#include "mqtt.hpp"
#include "smartconfig.hpp"
#include "utils.hpp"

#define SWITCH "/switch"

constexpr static auto TAG{"main"};

proto::MQTT mqttClient(CONFIG_MQTT_HOST, CONFIG_MQTT_USER, CONFIG_MQTT_PASS); // NOLINT

extern "C" void app_main() {
    std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
    esp_err_t res{ESP_OK};

    utils::configureIO();

    res = esp_event_loop_create_default();
    if (res != ESP_OK) {
        std::snprintf(buf.data(), buf.size(), "failed to create default event loop: (%s)", esp_err_to_name(res));
        LOG_E(TAG, buf.data());
        throw std::runtime_error(buf.data());
    }

    gpio_config_t gpioCfg{1ULL << GPIO_NUM_4 | 1ULL << GPIO_NUM_5, GPIO_MODE_OUTPUT, GPIO_PULLUP_DISABLE,
                          GPIO_PULLDOWN_ENABLE, GPIO_INTR_DISABLE};
    gpio_config(&gpioCfg); // cannot fail unless the config is messed up

    connection::initializeWifiThenExecute([]() {
        std::array<std::tuple<proto::MQTT::Topic, std::function<void(esp_mqtt_event_handle_t)>>, 2> handlers{
            {{{"/user/" CONFIG_MQTT_USER "/ns/public/housectl/switch/1/status", 0},
              [](esp_mqtt_event_handle_t event) {
                  if (utils::meansOn(event->data)) {
                      gpio_set_level(GPIO_NUM_4, 0);
                  } else if (utils::meansOff(event->data)) {
                      gpio_set_level(GPIO_NUM_4, 1);
                  }
              }},
             {{"/user/" CONFIG_MQTT_USER "/ns/public/housectl/switch/2/status", 0}, [](esp_mqtt_event_handle_t event) {
                  if (utils::meansOn(event->data)) {
                      gpio_set_level(GPIO_NUM_5, 0);
                  } else if (utils::meansOff(event->data)) {
                      gpio_set_level(GPIO_NUM_5, 1);
                  }
              }}}};
        mqttClient.connect();
        mqttClient.setTopicHandlers(handlers);
    });

    vTaskDelete(nullptr);
}
