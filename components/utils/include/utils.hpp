#pragma once

#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>
#include <memory>

namespace utils {
template<auto Fun>
struct DeleterFromFun {
    template<typename T>
    auto constexpr operator()(T &&arg) const noexcept -> void {
        Fun(std::forward<T>(arg));
    }
};

template<typename T, auto Fun>
using unique_ptr = std::unique_ptr<T, DeleterFromFun<Fun>>;

#if LOG_LOCAL_LEVEL >= 2
#define LOG_E(tag, format, ...)                                                                                        \
    utils::log_e_impl(__FILE__, __LINE__, (char const *)__func__, tag, format, ##__VA_ARGS__)

template<typename TagType, typename FormatStr, typename... Options>
auto log_e_impl(char const *filename, size_t line, char const *function, TagType const &tag, FormatStr const &format,
                Options &&...options) -> void {
    esp_log_write(ESP_LOG_ERROR, tag, "filename: %s, line: %d in function: %s", filename, line, function);
    esp_log_write(ESP_LOG_ERROR, tag, format, std::forward<Options>(options)...);
}
#else
#define LOG_E(tag, format, ...)
#endif

#if LOG_LOCAL_LEVEL >= 3
#define LOG_I(tag, format, ...)                                                                                        \
    utils::log_i_impl(__FILE__, __LINE__, (char const *)__func__, tag, format, ##__VA_ARGS__)

template<typename TagType, typename FormatStr, typename... Options>
auto log_i_impl(char const *filename, size_t line, char const *function, TagType const &tag, FormatStr const &format,
                Options &&...options) -> void {
    ESP_LOGE(tag, format, options...);
    esp_log_write(ESP_LOG_INFO, tag, "filename: %s, line: %d in function: %s", filename, line, function);
    esp_log_write(ESP_LOG_INFO, tag, format, std::forward<Options>(options)...);
}
#else
#define LOG_I(tag, format, ...)
#endif

#if CONFIG_LOG_DEFAULT_LEVEL >= 4
#define LOG_D(tag, format, ...)                                                                                        \
    utils::log_d_impl(__FILE__, __LINE__, (char const *)__func__, tag, format, ##__VA_ARGS__)

template<typename TagType, typename FormatStr, typename... Options>
auto log_d_impl(char const *filename, size_t line, char const *function, TagType const &tag, FormatStr const &format,
                Options &&...options) -> void {
    esp_log_write(ESP_LOG_DEBUG, tag, "filename: %s, line: %d in function: %s", filename, line, function);
    esp_log_write(ESP_LOG_DEBUG, tag, format, std::forward<Options>(options)...);
}
#else
#define LOG_D(tag, format, ...)
#endif

auto configureIO() noexcept -> esp_err_t;

auto initializeNVS() -> void;

auto runTimer(char const *pcTimerName, TickType_t xTimerPeriodInTicks, UBaseType_t uxAutoReload, void *pvTimerID,
              TimerCallbackFunction_t pxCallbackFunction, TickType_t xTicksToWait) noexcept -> BaseType_t;

auto meansOn(std::string_view text) noexcept -> bool;

auto meansOff(std::string_view text) noexcept -> bool;
} // namespace utils
