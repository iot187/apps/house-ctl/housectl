#include <FreeRTOS.h>
#include <cstring>
#include <driver/uart.h>
#include <esp_log.h>
#include <esp_vfs_dev.h>
#include <sdkconfig.h>

#include "utils.hpp"

namespace utils {
constexpr auto TAG{"helper_utils"};

auto configureIO() noexcept -> esp_err_t {
    static constexpr auto bufferSize{128};
    std::array<char, bufferSize> buf; // NOLINT

    errno = 0;
    if (setvbuf(stdin, nullptr, _IONBF, 0) != 0) {
        strerror_r(errno, buf.data(), bufferSize);
        LOG_E(TAG, "failed to setvbuf: (%s)", buf);
        return ESP_ERR_NOT_SUPPORTED;
    }

    errno = 0;
    if (setvbuf(stdout, nullptr, _IONBF, 0) != 0) {
        strerror_r(errno, buf.data(), bufferSize);
        LOG_E(TAG, "failed to setvbuf: (%s)", buf);
        return ESP_ERR_NOT_SUPPORTED;
    }

    if (uart_driver_install(static_cast<uart_port_t>(CONFIG_ESP_CONSOLE_UART_NUM), 256, 0, 0, nullptr, 0) != ESP_OK) {
        LOG_E(TAG, "failed to install uart driver");
        return ESP_ERR_INVALID_ARG;
    }
    esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);
    esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
    esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);
    return ESP_OK;
}

auto runTimer(char const *pcTimerName, TickType_t xTimerPeriodInTicks, UBaseType_t uxAutoReload, void *pvTimerID,
              TimerCallbackFunction_t pxCallbackFunction, TickType_t xTicksToWait) noexcept -> BaseType_t {
    auto *timerHandle{xTimerCreate(pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction)};
    if (timerHandle == nullptr) {
        LOG_E(TAG, "failed to create the timer");
        return pdFALSE;
    }
    return xTimerStart(timerHandle, xTicksToWait);
}

auto meansOn(std::string_view text) noexcept -> bool {
    return std::strncmp(text.data(), "1", 1) == 0 || std::strncmp(text.data(), "on", 2) == 0 ||
           std::strncmp(text.data(), "yes", 3) == 0 || std::strncmp(text.data(), "true", 4) == 0;
}

auto meansOff(std::string_view text) noexcept -> bool {
    return std::strncmp(text.data(), "0", 1) == 0 || std::strncmp(text.data(), "no", 2) == 0 ||
           std::strncmp(text.data(), "off", 3) == 0 || std::strncmp(text.data(), "false", 5) == 0;
}

} // namespace utils
