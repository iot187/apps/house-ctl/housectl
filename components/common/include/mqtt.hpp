#pragma once

#include <array>
#include <climits>
#include <functional>
#define SSIZE_MAX LONG_MAX
#include <mqtt_client.h>

#include "utils.hpp"

namespace proto {
class MQTT {
  public:
    struct alignas(32) Topic {
        Topic() : qos_(0) {}
        explicit Topic(std::string_view topic) : topic_(topic), qos_(0) {}
        Topic(std::string_view topic, int qos) : topic_(topic), qos_(qos) {}

        [[nodiscard]] auto getTopic() const noexcept { return topic_; }
        [[nodiscard]] auto getQos() const noexcept { return qos_; }

      private:
        std::string_view topic_;
        int qos_;
    };

    MQTT(std::string_view uri, std::string_view username, std::string_view password) noexcept;

    /**
     * @brief Set connection information
     *
     * @param host uri to connect to
     * @param username to authenticate
     * @param password [TODO:parameter]
     */
    auto setConnectionInfo(std::string_view host, std::string_view username,
                           std::string_view password) noexcept -> void;

    /**
     * @brief Use data provided to the object constructor to connect to the broker
     */
    auto connect() -> void;

    /**
     * @brief Register topic with possible handler functions to subscribe to these topics after connecting to the broker
     *
     * @param Topic Topic specification
     * @param handlers Handler function to use if data arrives with a topic matching the one used to subscribe
     */
    template<size_t Size>
    constexpr auto registerTopicHandlers(
        std::array<std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>>, Size> handlers) -> void {
        for (auto &handler : handlers) {
            handlers_.at(handlersSize_++) = std::move(handler);
        }
    }

    /**
     * @brief Register topic with possible handler functions to subscribe to these topics after connecting to the
     * broker deleting any previously existing handlers
     *
     * @param Topic Topic specification
     * @param handlers Handler function to use if data arrives with a topic matching the one used to subscribe
     */
    template<size_t Size>
    constexpr auto setTopicHandlers(
        std::array<std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>>, Size> handlers) -> void {
        handlersSize_ = 0;
        for (auto &handler : handlers) {
            handlers_.at(handlersSize_++) = std::move(handler);
        }
        handlers_.at(handlersSize_) = {};
    }

    /**
     * @brief Registers a default handler that executes for any topic that matches the base topic specified
     *
     * The default handler is a handler that only executes after failure to find any specific handler to a specific
     * topic.
     * It doesn't handle all topics though, only topics that start with the topic specified.
     * It can be made to match any topic and to be a general handler my specifying an empty topic so any topic will
     * match
     *
     * @note Topic specification specified here isn't used for subscription to the broker, it's only used as a fallback
     *
     * @param defaultHandler A tuple of topic specification and handler function
     */
    auto registerDefaultTopicHandler(
        std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>> defaultHandler) noexcept -> void {
        defaultHandler_ = std::move(defaultHandler);
    }

    [[nodiscard]] constexpr auto getHandlers() const
        -> std::array<std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>>, CONFIG_MQTT_MAX_HANDLERS> const
            & {
        return handlers_;
    };

    [[nodiscard]] constexpr auto getDefaultHandler() const
        -> std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>> const & {
        return defaultHandler_;
    }

    [[nodiscard]] auto operator*() const -> esp_mqtt_client_handle_t { return handle_.get(); }

  private:
    std::string_view host_{};
    std::string_view username_{};
    std::string_view password_{};
    utils::unique_ptr<esp_mqtt_client, esp_mqtt_client_destroy> handle_{};

    std::array<std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>>, CONFIG_MQTT_MAX_HANDLERS> handlers_{};
    std::tuple<Topic, std::function<void(esp_mqtt_event_handle_t)>> defaultHandler_{};
    unsigned int handlersSize_{};
};
} // namespace proto
