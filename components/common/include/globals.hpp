#pragma once

namespace globals {
constexpr static auto ERR_BUF_SIZE{64UL};

constexpr static auto NVS_MQTT_HOST_KEY{"mqttHost"};

constexpr static auto NVS_MQTT_USER_KEY{"mqttUser"};

constexpr static auto NVS_MQTT_PASS_KEY{"mqttPass"};

constexpr static auto NVS_PARTITION{"db"};

constexpr static auto NVS_NAMESPACE{"housectl"};

} // namespace globals
