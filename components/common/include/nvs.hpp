#pragma once

#include "globals.hpp"
#include "nvs.h"

#include <array>
#include <string_view>
#include <vector>

template<typename Tp>
struct is_i64 : std::false_type {};

template<>
struct is_i64<int64_t> : std::true_type {};

template<typename Tp>
struct is_u64 : std::false_type {};

template<>
struct is_u64<uint64_t> : std::true_type {};

template<typename Tp>
struct is_i32 : std::false_type {};

template<>
struct is_i32<int32_t> : std::true_type {};

template<typename Tp>
struct is_u32 : std::false_type {};

template<>
struct is_u32<uint32_t> : std::true_type {};

template<typename Tp>
struct is_i16 : std::false_type {};

template<>
struct is_i16<int16_t> : std::true_type {};

template<typename Tp>
struct is_u16 : std::false_type {};

template<>
struct is_u16<uint16_t> : std::true_type {};

template<typename Tp>
struct is_i8 : std::false_type {};

template<>
struct is_i8<int8_t> : std::true_type {};

template<typename Tp>
struct is_u8 : std::false_type {};

template<>
struct is_u8<uint8_t> : std::true_type {};

template<typename Tp>
struct is_string : std::false_type {};

template<>
struct is_string<std::string> : std::true_type {};

template<typename Tp>
struct is_blob : std::false_type {};

template<>
struct is_blob<std::vector<unsigned char>> : std::true_type {};

class NVS {
  public:
    friend auto swap(NVS &lhs, NVS &rhs) noexcept -> void;

    NVS(std::string_view partitionName, std::string_view appNamespace) noexcept;

    explicit NVS(std::string_view appNamespace) noexcept;

    NVS(const NVS &) = delete;

    NVS(NVS &&rhs) noexcept;

    auto operator=(const NVS &) -> NVS & = delete;

    auto operator=(NVS &&rhs) noexcept -> NVS &;

    ~NVS() noexcept;

    auto init() const -> void;

    auto open() const -> void;

    template<typename Tp, std::enable_if_t<is_i8<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_i8(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u8<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_u8(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i16<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_i16(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u16<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_u16(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i32<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_i32(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u32<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_u32(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i64<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_i64(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u64<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        Tp val{};
        auto res{nvs_get_u64(handle_, key.data(), &val)};
        if (res == ESP_OK || res == ESP_ERR_NVS_NOT_FOUND) {
            return val;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_string<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        size_t reqSize; // NOLINT
        auto res{nvs_get_str(handle_, key.data(), nullptr, &reqSize)};
        if (res == ESP_ERR_NVS_NOT_FOUND) {
            return {};
        }
        std::string string(reqSize, ' ');
        res = nvs_get_str(handle_, key.data(), string.data(), &reqSize);
        if (res == ESP_OK) {
            return string;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_blob<Tp>::value, bool> = true>
    auto get(std::string_view key) -> Tp {
        size_t reqSize; // NOLINT
        auto res{nvs_get_blob(handle_, key.data(), nullptr, &reqSize)};
        if (res == ESP_ERR_NVS_NOT_FOUND) {
            return {};
        }
        std::vector<unsigned char> string(reqSize, ' ');
        res = nvs_get_blob(handle_, key.data(), string.data(), &reqSize);
        if (res == ESP_OK) {
            return string;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i8<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_i8(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u8<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_u8(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i16<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_i16(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u16<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_u16(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i32<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_i32(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u32<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_u32(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_i64<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_i64(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_u64<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_u64(handle_, key.data(), val)};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_string<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_str(handle_, key.data(), val.data())};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    template<typename Tp, std::enable_if_t<is_blob<Tp>::value, bool> = true>
    auto set(std::string_view key, Tp val) -> void {
        auto res{nvs_set_blob(handle_, key.data(), val.data())};
        if (res == ESP_OK) {
            return;
        }
        std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
        std::snprintf(buf.data(), buf.size(), "failed to retrieve value: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

  private:
    std::string_view partitionName_;
    std::string_view appNamespace_;
    mutable nvs_handle_t handle_{};
};
