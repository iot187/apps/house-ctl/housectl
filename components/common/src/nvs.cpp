#include "nvs.hpp"

#include "nvs_flash.h"

auto swap(NVS &lhs, NVS &rhs) noexcept -> void {
    std::swap(lhs.appNamespace_, rhs.appNamespace_);
    std::swap(lhs.partitionName_, rhs.partitionName_);
    std::swap(lhs.handle_, rhs.handle_);
}

NVS::NVS(std::string_view partitionName, std::string_view appNamespace) noexcept
    : partitionName_(partitionName), appNamespace_(appNamespace) {}

NVS::NVS(std::string_view appNamespace) noexcept : partitionName_("nvs"), appNamespace_(appNamespace) {}

NVS::NVS(NVS &&rhs) noexcept
    : partitionName_(std::exchange(rhs.partitionName_, {})), appNamespace_(std::exchange(rhs.appNamespace_, {})),
      handle_(std::exchange(rhs.handle_, {})){};

auto NVS::operator=(NVS &&rhs) noexcept -> NVS & {
    std::exchange(this->partitionName_, rhs.partitionName_);
    std::exchange(this->appNamespace_, rhs.appNamespace_);
    std::exchange(this->handle_, rhs.handle_);
    return *this;
}

NVS::~NVS() noexcept { nvs_close(handle_); }

auto NVS::init() const -> void {
    auto res{nvs_flash_init_partition(partitionName_.data())};
    std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT

    if (res != ESP_OK) {
        std::snprintf(buf.data(), buf.size(), "failed to initialize %s partition: (%s)", partitionName_.data(),
                      esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }
}

auto NVS::open() const -> void {
    esp_err_t res;                               // NOLINT
    std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
    res = nvs_open(appNamespace_.data(), NVS_READWRITE, &handle_);
    if (res != ESP_OK) {
        std::snprintf(buf.data(), buf.size(), "failed to open %s partition: (%s)", partitionName_.data(),
                      esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }
}
