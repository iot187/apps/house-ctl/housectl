#include <cstring>
#include <sdkconfig.h>
#include <string_view>

#include "globals.hpp"
#include "mqtt.hpp"

constexpr static auto TAG{"mqtt_ctrl"};
extern const uint8_t rootca_pem_start[] asm("_binary_rootca_pem_start"); // NOLINT

namespace {
auto mqttEventHandler(void *handlerArg, esp_event_base_t eventBase, int32_t eventId, void *eventData) -> void;
} // namespace

namespace proto {
MQTT::MQTT(std::string_view uri, std::string_view username, std::string_view password) noexcept
    : host_(uri), username_(username), password_(password), handlers_() {}

auto MQTT::connect() -> void {
    std::array<char, globals::ERR_BUF_SIZE> buf; // NOLINT
    esp_err_t res;                               // NOLINT

    esp_mqtt_client_config_t const cfg{.uri = host_.data(), .username = username_.data(), .password = password_.data(),
                                       /* .cert_pem = reinterpret_cast<char const *>(rootca_pem_start) */};
    // cfg.uri = host_.data();
    // cfg.username = username_.data();
    // cfg.password = password_.data();
    // cfg.cert_pem = reinterpret_cast<char const *>(rootca_pem_start); // NOLINT

    handle_.reset(esp_mqtt_client_init(&cfg));
    if (handle_ == nullptr) {
        throw std::runtime_error("mqtt initialization failure");
    }

    res = esp_mqtt_client_register_event(handle_.get(), MQTT_EVENT_ANY, mqttEventHandler, const_cast<MQTT *>(this));
    if (res != ESP_OK) {
        std::snprintf(buf.data(), buf.size(), "failed to register event handler for mqtt: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }

    res = esp_mqtt_client_start(handle_.get());
    fprintf(stderr, "After starting the client\n");
    if (res != ESP_OK) {
        std::snprintf(buf.data(), buf.size(), "failed to start mqtt client: (%s)", esp_err_to_name(res));
        throw std::runtime_error(buf.data());
    }
}

auto MQTT::setConnectionInfo(std::string_view host, std::string_view username,
                             std::string_view password) noexcept -> void {
    host_ = host;
    username_ = username;
    password_ = password;
}
} // namespace proto

namespace {
auto subscribeToTopics(proto::MQTT const &mqttClient, esp_mqtt_event_handle_t event) -> void;

auto handleData(proto::MQTT const &mqttClient, esp_mqtt_event_handle_t event) -> void;

auto mqttEventHandler(void *handlerArg, esp_event_base_t /*eventBase*/, int32_t eventId, void *eventData) -> void {
    switch (eventId) {
    case MQTT_EVENT_BEFORE_CONNECT:
        break;
    case MQTT_EVENT_CONNECTED:
    case MQTT_EVENT_UNSUBSCRIBED:
        subscribeToTopics(*static_cast<proto::MQTT *>(handlerArg), static_cast<esp_mqtt_event_handle_t>(eventData));
        break;
        break;
    case MQTT_EVENT_SUBSCRIBED:
    case MQTT_EVENT_PUBLISHED:
        break;
    case MQTT_EVENT_DATA:
        handleData(*static_cast<proto::MQTT *>(handlerArg), static_cast<esp_mqtt_event_handle_t>(eventData));
        break;
    case MQTT_EVENT_ERROR:
    case MQTT_EVENT_DISCONNECTED:
    default:
        esp_mqtt_client_reconnect(**static_cast<proto::MQTT *>(handlerArg));
        break;
    }
}

auto subscribeToTopics(proto::MQTT const &mqttClient, esp_mqtt_event_handle_t event) -> void {
    for (auto const &handler : mqttClient.getHandlers()) {
        auto const &[topic, func] = handler;
        if (topic.getTopic().empty()) {
            return;
        }
        if (esp_mqtt_client_subscribe(event->client, topic.getTopic().data(), topic.getQos()) == -1) {
            // LOG_E(TAG, "failed to subscribe to topic: %s, qos: %d", topic.getQos(), topic.getQos());
        }
    }
}

auto handleData(proto::MQTT const &mqttClient, esp_mqtt_event_handle_t event) -> void {
    for (auto const &handler : mqttClient.getHandlers()) {
        auto const &[topic, func] = handler;
        if (std::strncmp(event->topic, topic.getTopic().data(), event->topic_len) == 0) {
            if (func) {
                func(event);
            }
            return;
        }

        if (topic.getTopic().empty()) {
            auto const &[topic, func] = mqttClient.getDefaultHandler();
            if (topic.getTopic().empty() || std::basic_string_view(event->data).rfind(topic.getTopic()) == 0) {
                if (func) {
                    func(event);
                }
                return;
            }

            return;
        }
    }
}
} // namespace
