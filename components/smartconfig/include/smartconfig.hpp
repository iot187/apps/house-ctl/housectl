#pragma once

#include <functional>

namespace connection {
auto initializeWifi() noexcept -> int;

auto initializeWifiThenExecute(std::function<void()> callback) noexcept -> int;

} // namespace connection
