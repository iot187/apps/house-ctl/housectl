#include "smartconfig.hpp"
#include <chrono>
#include <climits>
#include <cstring>

#define SSIZE_MAX LONG_MAX
#include <esp_netif.h>

#include <esp_smartconfig.h>
#include <esp_wifi.h>
#include <freertos/event_groups.h>
#include <freertos/timers.h>
#include <sdkconfig.h>

#include "utils.hpp"

static constexpr auto TAG{"smartconfig_ctrl"};

namespace {

struct alignas(64) AppState {
    constexpr static auto SmartConfigReconnectDelayTicks{10000 / portTICK_PERIOD_MS};
    constexpr static auto SmartConfigEnable{1U << 0U};
    constexpr static auto SmartConfigDisable{1U << 1U};
    constexpr static auto WifiCheckDelaySecs{5};

    std::function<void()> applicationStartCb{};
    TaskHandle_t smartConfigTaskHandle{};
    bool smartConfigRunning{};
    bool applicationRunning{};
};

AppState state{}; // NOLINT

/**
 * @brief Disable smartconfig
 */
inline auto disableSmartConfig() noexcept -> void;

/**
 * @brief Enables smartconfig
 */
inline auto enableSmartConfig() noexcept -> void;

/**
 * @brief Use credentials acquired from smartconfig to configure wifi
 *
 * @param eventData data acquired from smartconfig
 */
auto handleSmartConfigData(smartconfig_event_got_ssid_pswd_t *eventData) noexcept -> void;

/**
 * @brief Helper function to connect to wifi
 */
inline auto connectToWifi() noexcept -> int;

/**
 * @brief Checks wifi connectivity after specified amount of seconds and if not connected enables smart config
 *
 * @tparam Rep an arithmetic type representing the number of ticks
 * @return Ratio a std::ratio representing the tick period (i.e. the number of second's fractions per tick)
 */
template<typename Rep, typename Period = std::ratio<1>>
constexpr auto checkWifiAfter(std::chrono::duration<Rep, Period> period) noexcept -> void {
    utils::runTimer(
        "checkWifi", std::chrono::duration_cast<std::chrono::milliseconds>(period).count() / portTICK_PERIOD_MS,
        pdFALSE, nullptr,
        [](xTimerHandle timerHandle) constexpr noexcept {
            xTimerDelete(timerHandle, 0);
            wifi_bandwidth_t wifiBandwidth; // NOLINT
            if (esp_wifi_get_bandwidth(ESP_IF_WIFI_STA, &wifiBandwidth) != ESP_OK) {
                enableSmartConfig();
            }
        },
        0);
}

template<typename Rep, typename Period = std::ratio<1>>
constexpr auto checkIpAfter(std::chrono::duration<Rep, Period> period) noexcept -> void {
    utils::runTimer(
        "checkIP", std::chrono::duration_cast<std::chrono::milliseconds>(period).count() / portTICK_PERIOD_MS,
        pdFALSE, nullptr,
        [](xTimerHandle timerHandle) constexpr noexcept {
            xTimerDelete(timerHandle, 0);
            wifi_bandwidth_t wifiBandwidth; // NOLINT
            if (esp_wifi_get_bandwidth(ESP_IF_WIFI_STA, &wifiBandwidth) != ESP_OK) {
                enableSmartConfig();
            }
        },
        0);
}

/**
 * @brief Starts the application by calling the callback function only once
 */
auto startApplication(bool once = false) noexcept -> void;

auto connEventHandler(void * /*arg*/, esp_event_base_t eventBase, int32_t eventId, void *eventData) noexcept -> void {
    if (eventBase == WIFI_EVENT) {
        switch (eventId) {
        case WIFI_EVENT_STA_START:
            connectToWifi();
            checkWifiAfter(std::chrono::seconds(AppState::WifiCheckDelaySecs));
            break;
        case WIFI_EVENT_STA_CONNECTED:
            disableSmartConfig();
            break;
        case WIFI_EVENT_STA_DISCONNECTED:
            connectToWifi();
            enableSmartConfig();
            break;
        default:
            LOG_E(TAG, "got an unsupportted event id: %d", eventId);
            break;
        }
    } else if (eventBase == IP_EVENT) {
        switch (eventId) {
        case IP_EVENT_STA_GOT_IP:
        case IP_EVENT_GOT_IP6:
            startApplication();
            break;
        default:
            LOG_E(TAG, "got an unsupportted event id: %d", eventId);
            break;
        }
    } else if (eventBase == SC_EVENT) {
        switch (eventId) {
        case SC_EVENT_SCAN_DONE:
        case SC_EVENT_FOUND_CHANNEL:
            break;
        case SC_EVENT_GOT_SSID_PSWD:
            disableSmartConfig();
            handleSmartConfigData(static_cast<smartconfig_event_got_ssid_pswd_t *>(eventData));
            break;
        default:
            LOG_E(TAG, "got an unsupportted event id: %d", eventId);
            break;
        }
    }
}

auto smartConfigTask(void * /* pvParameters */) noexcept -> void {
    esp_err_t res; // NOLINT

    esp_smartconfig_set_type(static_cast<smartconfig_type_t>(CONFIG_ESP_SMARTCONFIG_TYPE));

    smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
    while (true) {
        uint32_t notificationValue; // NOLINT
        if (xTaskNotifyWait(0, ULONG_MAX, &notificationValue, ULONG_MAX) != pdPASS) {
            continue;
        }
        if (notificationValue == AppState::SmartConfigDisable) {
            res = esp_smartconfig_stop();
            if (res != ESP_OK) {
                LOG_E(TAG, "failed to stop smartconfig: (%s)", esp_err_to_name(res));
                continue;
            }
            state.smartConfigRunning = false;
        } else if (notificationValue == AppState::SmartConfigEnable && !state.smartConfigRunning) {
            res = esp_smartconfig_start(&cfg);
            if (res != ESP_OK) {
                LOG_E(TAG, "failed to start smartconfig: (%s)", esp_err_to_name(res));
                continue;
            }
            state.smartConfigRunning = true;

            // after some time disable smart config to retry to connect to wifi
            if (utils::runTimer(
                    "retryConnection", AppState::SmartConfigReconnectDelayTicks, pdFALSE, nullptr,
                    [](xTimerHandle timerHandle) {
                        xTimerDelete(timerHandle, 0);
                        disableSmartConfig();
                        connectToWifi();
                        checkWifiAfter(std::chrono::seconds(AppState::WifiCheckDelaySecs));
                    },
                    0) != pdTRUE) {
                LOG_E(TAG, "failed to create timer to disable smart config after predefined time");
            }
        }
    }
}

inline auto disableSmartConfig() noexcept -> void {
    if (state.smartConfigTaskHandle != nullptr) {
        xTaskNotify(state.smartConfigTaskHandle, AppState::SmartConfigDisable,
                    eSetValueWithOverwrite); // cannot fail
    }
}

inline auto enableSmartConfig() noexcept -> void {
    if (state.smartConfigTaskHandle != nullptr && !state.smartConfigRunning) {
        xTaskNotify(state.smartConfigTaskHandle, AppState::SmartConfigEnable,
                    eSetValueWithOverwrite); // cannot fail
    }
}

auto handleSmartConfigData(smartconfig_event_got_ssid_pswd_t *event) noexcept -> void {
    esp_err_t res; // NOLINT

    wifi_config_t cfg{};
    std::memcpy(static_cast<void *>(cfg.sta.ssid), static_cast<void const *>(event->ssid), sizeof(cfg.sta.ssid));
    std::memcpy(static_cast<void *>(cfg.sta.password), static_cast<void const *>(event->password),
                sizeof(cfg.sta.password));
    cfg.sta.bssid_set = event->bssid_set;
    if (cfg.sta.bssid_set) {
        std::memcpy(static_cast<void *>(cfg.sta.bssid), static_cast<void const *>(event->bssid), sizeof(cfg.sta.bssid));
    }
    cfg.sta.scan_method = WIFI_ALL_CHANNEL_SCAN;
    cfg.sta.sort_method = WIFI_CONNECT_AP_BY_SIGNAL;

    res = esp_wifi_set_config(WIFI_IF_STA, &cfg);
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to set wifi config: (%s)", esp_err_to_name(res));
        return;
    }

    res = esp_wifi_connect();
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to connect to wifi: (%s)", esp_err_to_name(res));
        return;
    }
}

inline auto connectToWifi() noexcept -> int {
    auto res{esp_wifi_connect()};

    if (res != ESP_OK) {
        LOG_E(TAG, "failed to connect to wifi: (%s)", esp_err_to_name(res));
    }

    return res;
}

auto startApplication(bool once) noexcept -> void {
    if (once) {
        if (!state.applicationRunning && state.applicationStartCb) {
            state.applicationStartCb();
        }
    } else {
        if (state.applicationStartCb) {
            state.applicationStartCb();
        }
    }
    state.applicationRunning = true;
}
} // namespace

auto connection::initializeWifi() noexcept -> int {
    esp_err_t res; // NOLINT

    esp_netif_init();
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    res = esp_wifi_init(&cfg);

    if (res != ESP_OK) {
        LOG_E(TAG, "failed to init wifi: (%s)", esp_err_to_name(res));
        return res;
    }

    esp_wifi_set_mode(WIFI_MODE_STA); // cannot fail

    res = esp_wifi_set_ps(WIFI_PS_NONE);
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to set wifi power saving mode: (%s)", esp_err_to_name(res));
        return res;
    }

    res = esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, connEventHandler, nullptr);
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to register event handler: (%s)", esp_err_to_name(res));
        return res;
    }

    res = esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, connEventHandler, nullptr);
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to register event handler: (%s)", esp_err_to_name(res));
        return res;
    }

    res = esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, connEventHandler, nullptr);
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to register event handler: (%s)", esp_err_to_name(res));
        return res;
    }

    res = esp_wifi_start();
    if (res != ESP_OK) {
        LOG_E(TAG, "failed to start wifi: (%s)", esp_err_to_name(res));
        return res;
    }

    if (xTaskCreate(smartConfigTask, "smartConfigTask", 2048, nullptr, 3, &state.smartConfigTaskHandle) != pdPASS) {
        LOG_E(TAG, "failed to create the smartconfig task");
    }
    return 0;
}

auto connection::initializeWifiThenExecute(std::function<void()> callback) noexcept -> int {
    state.applicationStartCb = std::move(callback);
    return initializeWifi();
}
